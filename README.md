
## Links related to tooling

- [.NET Core](https://www.microsoft.com/net/download/core)
- [.NET Core docs](https://docs.microsoft.com/en-us/dotnet/articles/core/index)
- [.NET Core command-line interface (CLI) tools](https://docs.microsoft.com/en-us/dotnet/articles/core/tools/)

### How to run
* Download and install .Net Core
* Clone Repository and go to web project
* inside web project run command 'dotnet build'


Command 'dotnet build' should run Before UnitTest and After create Nuget Package.


